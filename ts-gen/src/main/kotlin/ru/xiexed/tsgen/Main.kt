package ru.xiexed.tsgen

import com.intellij.openapi.util.Disposer
import com.intellij.openapi.vfs.StandardFileSystems
import com.intellij.openapi.vfs.VirtualFileManager
import com.intellij.openapi.vfs.local.CoreLocalFileSystem
import com.intellij.psi.PsiManager
import com.intellij.psi.SingleRootFileViewProvider
import org.jetbrains.kotlin.cli.common.CLIConfigurationKeys
import org.jetbrains.kotlin.cli.common.messages.MessageCollector
import org.jetbrains.kotlin.cli.jvm.compiler.*
import org.jetbrains.kotlin.config.CompilerConfiguration
import org.jetbrains.kotlin.config.JVMConfigurationKeys
import org.jetbrains.kotlin.lexer.KtTokens
import org.jetbrains.kotlin.psi.*
import java.io.File

private fun acceptFiles(kotlinFiles: Collection<File>, ktTreeVisitor: KtTreeVisitor<Void>) {
    val disposable = Disposer.newDisposable()
    try {
        val config = CompilerConfiguration()
        config.put(JVMConfigurationKeys.NO_JDK, true)
        config.put(CLIConfigurationKeys.MESSAGE_COLLECTOR_KEY, MessageCollector.NONE)
        val configFiles = EnvironmentConfigFiles.JVM_CONFIG_FILES
        val environment = KotlinCoreEnvironment.createForProduction(disposable, config, configFiles)
        val psiManager = PsiManager.getInstance(environment.project)
        val fileManager = VirtualFileManager.getInstance()
        val localFS = fileManager.getFileSystem(StandardFileSystems.FILE_PROTOCOL) as CoreLocalFileSystem

        for (file in kotlinFiles) {
            val virtualFile = localFS.findFileByIoFile(file)!!
            for (psiFile in SingleRootFileViewProvider(psiManager, virtualFile).allFiles) {
                if (psiFile !is KtFile) continue
                psiFile.accept(ktTreeVisitor)
            }
        }
    } finally {
        disposable.dispose()
    }
}

private val KtParameter.tsType: String
    get() {
        val text = typeReference?.text ?: return "any"
        return typeMap[text] ?: text
    }


private val typeMap = mapOf(
        "String" to "string",
        "Int" to "number",
        "Long" to "number",
        "Float" to "number",
        "Double" to "number"
)

fun main(args: Array<String>) {
    val tsClassesTexts = mutableListOf<String>()
    acceptFiles(args.map(::File), TsWriter(tsClassesTexts))
    tsClassesTexts.forEach(::println)
}

class TsWriter(private val result: MutableList<String>) : KtTreeVisitor<Void>() {

    private val fieldIdent = "\n" + " ".repeat(4)

    override fun visitClassOrObject(cls: KtClassOrObject, data: Void?): Void? {
        if (!cls.hasModifier(KtTokens.DATA_KEYWORD)) return data
        val valueParameterList = cls.primaryConstructor?.valueParameterList?.parameters ?: return data
        result.add("""export class ${cls.name} {
                    |    ${valueParameterList.joinToString(fieldIdent) { with(it) { "public $name: $tsType;" } }}
                    |}""".trimMargin())

        return data
    }
}

