import { Component, OnInit } from '@angular/core';
import { WebsocketService } from "@core/websocket/websocket.service";
import { Canceled } from "@core/model/Canceled";
import { EventType } from "@core/websocket/websocket.enum";

@Component({
    selector: 'app-canceled',
    templateUrl: './canceled.component.html',
    styleUrls: ['./canceled.component.css']
})
export class CanceledComponent implements OnInit {

    public data = [];

    constructor(private socket: WebsocketService) {
    }

    public ngOnInit() {
        this.socket.on<Canceled>(EventType.Canceled)
            .subscribe((data) => this.data.push(data));
    }

}
