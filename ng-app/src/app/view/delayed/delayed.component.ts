import { Component, OnInit } from '@angular/core';
import { WebsocketService } from "@core/websocket/websocket.service";
import { Delayed } from "@core/model/Delayed";
import { EventType } from "@core/websocket/websocket.enum";

@Component({
    selector: 'app-delayed',
    templateUrl: './delayed.component.html',
    styleUrls: ['./delayed.component.css']
})
export class DelayedComponent implements OnInit {

    public data = [];

    constructor(private socket: WebsocketService) {
    }

    public ngOnInit() {
        this.socket.on<Delayed>(EventType.Delayed)
            .subscribe((data) => this.data.push(data));
    }

}
