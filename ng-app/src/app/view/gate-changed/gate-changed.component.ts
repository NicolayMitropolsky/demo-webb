import { Component, OnInit } from '@angular/core';
import { WebsocketService } from "@core/websocket/websocket.service";
import { GateChanged } from "@core/model/GateChanged";
import { EventType } from "@core/websocket/websocket.enum";

@Component({
    selector: 'app-gate-changed',
    templateUrl: './gate-changed.component.html',
    styleUrls: ['./gate-changed.component.css']
})
export class GateChangedComponent implements OnInit {

    public data = [];

    constructor(private socket: WebsocketService) {
    }

    public ngOnInit() {
        this.socket.on<GateChanged>(EventType.GateChanged)
            .subscribe((data) => this.data.push(data));
    }

}
