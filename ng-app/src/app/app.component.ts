import { Component } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "@env/environment";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    public flightData = { "flightId": "999" };
    public timeUpdate = 10000;
    private interval;

    constructor(private http: HttpClient) {

    }

    private setNewInterval() {
        clearInterval(this.interval);
        this.interval = setInterval(() => {
            this.updateFlights();
        }, this.timeUpdate);
    }

    public get flightDataValue() {
        return JSON.stringify(this.flightData, null, 2);
    }

    public set flightDataValue(v) {
        try {
            this.flightData = JSON.parse(v);
        }
        catch (e) {
            console.log('error while you were typing the JSON');
        }
    }

    public updateFlights() {
        this.http.post(environment.httpApi + '/updateFlight', this.flightData)
            .subscribe();
    }

}
