import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WebsocketModule } from "@core/websocket/websocket.module";
import { WEBSOCKET_CONFIG } from "@core/websocket/websocket.config";
import { ErrorHandlerService } from "@core/error-handler/error-handler.service";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { CanceledComponent } from './view/canceled/canceled.component';
import { DelayedComponent } from './view/delayed/delayed.component';
import { GateChangedComponent } from './view/gate-changed/gate-changed.component';

@NgModule({
    declarations: [
        AppComponent,
        CanceledComponent,
        DelayedComponent,
        GateChangedComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        WebsocketModule.forRoot(WEBSOCKET_CONFIG)
    ],
    providers: [
        {
            provide: ErrorHandler,
            useClass: ErrorHandlerService,
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
