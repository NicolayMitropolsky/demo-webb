import { Inject, Injectable } from '@angular/core';
import { WebsocketConfig } from "./websocket.config";
import { WebSocketSubject } from "rxjs/webSocket";
import { SocketMessage } from "@core/websocket/websocket.config";
import { catchError, filter, map, retry } from "rxjs/operators";
import { Observable } from "rxjs/internal/Observable";
import { throwError } from "rxjs/internal/observable/throwError";

@Injectable()
export class WebsocketService {
    public socket$: WebSocketSubject<SocketMessage<object>> = null;

    constructor(@Inject('config') private config: WebsocketConfig) {
        this.init(config);
    }

    public on<T = object>(eventType: string): Observable<T> {
        return this.socket$.pipe(
            filter((message: SocketMessage<T>) => message.type === eventType),
            map((message: SocketMessage<T>) => message.data),
            catchError((err) => throwError(err)),
            retry()
        );
    }

    private init(config: WebsocketConfig) {
        this.socket$ = new WebSocketSubject(config.host);
    }

}
