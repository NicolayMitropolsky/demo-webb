import { environment } from "@env/environment";

export interface WebsocketConfig {
    host: string;
}

export const WEBSOCKET_CONFIG: WebsocketConfig = {
    host: environment.wsApi
};

export interface SocketMessage<T = object> {
    type: string;
    data: T;
}