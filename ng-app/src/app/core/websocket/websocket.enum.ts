export enum EventType {
    Canceled = "Canceled",
    GateChanged = "GateChanged",
    Delayed = "Delayed",
}