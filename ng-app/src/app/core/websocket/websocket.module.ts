import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsocketService } from "./websocket.service";

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [WebsocketService],
    declarations: []
})
export class WebsocketModule {
    static forRoot(config): ModuleWithProviders {
        return {
            ngModule: WebsocketModule,
            providers: [
                WebsocketService,
                { provide: 'config', useValue: config }
            ]
        };
    }
}
