export class GateChanged {
    public flightId: string;
    public gate: string;
}
