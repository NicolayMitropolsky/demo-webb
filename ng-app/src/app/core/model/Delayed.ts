export class Delayed {
    public flightId: string;
    public departure: number;
    public arrival: number;
}
