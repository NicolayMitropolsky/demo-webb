import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class ErrorHandlerService implements ErrorHandler {
    public handleError(error: Error) {
        console.error('It happens: ', error);
    }
}
