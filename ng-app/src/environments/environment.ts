export const environment = {
    production: false,
    httpApi: 'http://192.168.1.69:8080',
    wsApi: 'ws://192.168.1.69:8080/flightsUpdates'
};
