package ru.xiexed.demowebb

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter
import reactor.core.publisher.Flux

@Component
class WebSocketsConfig
@Autowired constructor(var eventBus: Flux<FlightChangeEvent>, val objectMapper: ObjectMapper) {

    @Bean
    fun webSocketMapping() = SimpleUrlHandlerMapping().apply {
        order = 1
        urlMap = mapOf("/flightsUpdates" to webSocketHandler())
    }

    @Bean
    fun webSocketHandler() = WebSocketHandler { session ->
        session.send(eventBus.map { Event(it.javaClass.simpleName, it) }
                .map(objectMapper::writeValueAsString)
                .map(session::textMessage))
    }

    @Bean
    fun handlerAdapter() = WebSocketHandlerAdapter()

}

data class Event(val type: String, val data: FlightChangeEvent)