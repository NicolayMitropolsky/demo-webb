package ru.xiexed.demowebb

import kotlinx.coroutines.experimental.reactive.awaitFirst
import kotlinx.coroutines.experimental.reactive.awaitFirstOrNull
import reactor.core.publisher.*
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlinx.coroutines.experimental.reactor.*

class FlightStorage {

    private val storage = ConcurrentHashMap<String, Flight>()

    fun getFlights(): Flux<Flight> = storage.values.toFlux()

    fun getFlight(id: String) = storage[id]?.toMono() ?: Mono.empty()

    fun addFlight(flightMono: Mono<Flight>): Mono<Void> =
            flightMono.doOnNext { flight -> storage[flight.flightId] = flight }.then()

}


class FlightCommandsProcessor(
        private val storage: FlightStorage,
        private val eventBus: FluxProcessor<FlightChangeEvent, FlightChangeEvent>
) {

    private val rand = Random()

    fun receiveCommand(newFlightInfo: Flight): Mono<Void> = mono{
         val prevFlight = storage.getFlight(newFlightInfo.flightId).awaitFirstOrNull()
         if(prevFlight == null){
             storage.addFlight(Mono.just(newFlightInfo)).awaitFirstOrNull()
             eventBus.onNext(Add(newFlightInfo))
         } else {
             val diffEvent = getDiffEvent(newFlightInfo.flightId, prevFlight, newFlightInfo)
             eventBus.onNext(diffEvent)
         }
     }.then()


    private fun getDiffEvent(flightId: String, prevFlight: Flight?, newFlight: Flight?): FlightChangeEvent {
//        if(prevFlight == null && newFlight != null)
//            return Add(newFlight)
//        if(prevFlight != null && newFlight == null)
//            return Delete(prevFlight.flightId)

        return when (rand.nextInt(3)) {
            0 -> Canceled(flightId)
            1 -> Delayed(flightId, 0, System.currentTimeMillis())
            2 -> GateChanged(flightId, "G09")
            else -> error("never happen")
        }

    }


}
