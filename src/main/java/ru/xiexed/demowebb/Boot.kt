package ru.xiexed.demowebb

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.*

@SpringBootApplication
class Boot {

    @Bean
    fun flightChangesFlux() = TopicProcessor.create<FlightChangeEvent>()

    @Bean
    fun fightStorage() = FlightStorage()

    @Bean
    fun commandsProcessor() = FlightCommandsProcessor(fightStorage(), flightChangesFlux())

}

@RestController
class Cont(val commandsProcessor: FlightCommandsProcessor, val storage: FlightStorage) {

    @GetMapping("/flights")
    fun getFlights(): Flux<Flight> = storage.getFlights()

    @CrossOrigin(origins = ["*"])
    @PostMapping("/updateFlight")
    fun updateFlight(@RequestBody newFlightInfo: Mono<Flight>): Mono<Void> =
            newFlightInfo.flatMap { commandsProcessor.receiveCommand(it) }.then()

}

fun main(args: Array<String>) {
    SpringApplication.run(Boot::class.java, *args)
}


data class Flight(val flightId: String,
                  val origin: String
//                  val destination: String,
//                  val departure: Long,
//                  val arrival: Long,
//                  val gate: String
)

interface FlightChangeEvent

data class Add(val data: Flight) : FlightChangeEvent

data class Delete(val flightId: Array<String>) : FlightChangeEvent

data class Canceled(val flightId : String) : FlightChangeEvent

data class Delayed(val flightId : String, val departure: Long, val arrival: Long): FlightChangeEvent

data class GateChanged(val flightId : String, val gate: String): FlightChangeEvent